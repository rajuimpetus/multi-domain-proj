///////////////////////////////////////////////////
// imports

import { Component } from "react";
import "../styles/help.module.css"

///////////////////////////////////////////////////
// component

export default class Home extends Component{

	///////////////////////////////////////////////////
	// constructor
	constructor(props){
		
		super(props);
		this.state = {
			bgColor: [{
				color: 'red',
				name: 'red'
			},
			{
				color: 'green',
				name: 'green'
			},
			{
				color: 'yellow',
				name: 'yellow'
			},
			{
				color: 'black',
				name: 'black'
			}],
			currentSelectedBgColor: 'red'
		}
	}

	///////////////////////////////////////////////////
	// render

	render(){
		return(
			<div className="container">
			  <div className="mainContent">
				<p className="mainContentText">Select background color</p>
				{
					this.state.bgColor.map((item, index)=>{
						
						if(item.color !== this.state.currentSelectedBgColor ){
							return(
								<div className="button" onClick={()=>{this.handleUpdateBgColor(item.color)}}>{item.name}</div>
							)
						}
					})
				}
			  </div>
			</div>
		)
	}

	///////////////////////////////////////////////////
	// actions: user
	 
	handleUpdateBgColor(color){

		this.setState({currentSelectedBgColor: color})
	}
  
}
