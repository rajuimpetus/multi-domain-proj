///////////////////////////////////////////////////
// imports

import { Component } from "react";
import styled from "styled-components"
import CheckHostName from "../components/ChechHostName"

///////////////////////////////////////////////////
// component

export default class Home extends Component{

	///////////////////////////////////////////////////
	// constructor
	constructor(props){
		
		super(props);
		this.state = {
			bgColor: [{
				color: 'red',
				name: 'red'
			},
			{
				color: 'green',
				name: 'green'
			},
			{
				color: 'yellow',
				name: 'yellow'
			},
			{
				color: 'black',
				name: 'black'
			}],
			currentSelectedBgColor: 'red'
		}
	}

	///////////////////////////////////////////////////
	// render

	render(){
		let host = this.props.host
		return(
			<Container bgColor={this.state.currentSelectedBgColor} >
			  <MainContent>
				  <CheckHostName host={host} />
				<MainContentText className="checkMyVisi">Select background color</MainContentText>
				{
					this.state.bgColor.map((item, index)=>{
						
						if(item.color !== this.state.currentSelectedBgColor ){
							return(
								<Button onClick={()=>{this.handleUpdateBgColor(item.color)}}>{item.name}</Button>
							)
						}
					})
				}
			  </MainContent>
			</Container>
		)
	}

	///////////////////////////////////////////////////
	// actions: user
	 
	handleUpdateBgColor(color){

		this.setState({currentSelectedBgColor: color})
	}
  
}


export async function getServerSideProps({ req, res }) {
	res.setHeader(
	  'Cache-Control',
	  'public, s-maxage=1, stale-while-revalidate=59'
	);
  
	return {
	  props: {
		host: req.headers.host
	  }
	};
  }
///////////////////////////////////////////////////
// styled component 

const Container = styled.div`

  height: 100vh;
  width: 100vw;
  background-color: ${props => props.bgColor ? props.bgColor : "purpule"};
`;

const MainContent = styled.div`

	height: inherit;

	display: flex;
	justify-content: center;
	align-items: center;
	align-self: center;
`;

const MainContentText = styled.p`

	color: white;
`;

const Button = styled.div`

	border: 1px solid white;
	height: 34px;
	padding: 5px;
	margin-left: 10px;
	color: white;

	cursor: pointer;
`;