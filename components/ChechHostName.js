///////////////////////////////////////////////////
// imports

import { Component } from "react";

///////////////////////////////////////////////////
// component

export default class CheckHostName extends Component{

	///////////////////////////////////////////////////
	// constructor
	constructor(props){
		
		super(props);
		this.state = {
		}
	}

	///////////////////////////////////////////////////
	// render

	render(){
		let host = this.props.host
		return(
			<div>
               Host Name =  {this.props.host}
            </div>
		)
	}

	///////////////////////////////////////////////////
	// actions: user
	 
	handleUpdateBgColor(color){

		this.setState({currentSelectedBgColor: color})
	}
  
}

